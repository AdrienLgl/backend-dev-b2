package com.campusacademy.b2dev.backenddevelopmentsupport.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("visitor").password(this.passwordEncoder.encode("test")).roles("VISITOR")
                .and()
                .withUser("user").password(this.passwordEncoder.encode("test")).authorities("ROLE_USER")
                .and()
                .withUser("admin").password(this.passwordEncoder.encode("test")).roles("USER", "ADMIN");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                // Connection httpBasic
                .httpBasic().and()
                // Connection formulaire
                .formLogin().and()
                .logout().deleteCookies("JSESSIONID").invalidateHttpSession(true).and()
                .authorizeRequests()
                .antMatchers(
                        "/login",
                        "/swagger-resources/**",
                        "/swagger-ui/**",
                        "/v2/api-docs",
                        "/webjars/**"
                ).permitAll()
                // .antMatchers("/users").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT).hasAnyAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.POST).hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/powers/**").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.DELETE).hasAnyRole("ADMIN")
                .anyRequest().authenticated();
    }

}

