package com.campusacademy.b2dev.backenddevelopmentsupport.api.mapper;

import com.campusacademy.b2dev.backenddevelopmentsupport.api.model.PowerDTO;
import com.campusacademy.b2dev.backenddevelopmentsupport.model.Power;
import com.campusacademy.b2dev.backenddevelopmentsupport.model.SuperHero;
import lombok.NonNull;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class PowerMapper {

    @Named(value = "getSize")
    public int getSize(@NonNull List<SuperHero> superHeroes) {
        return superHeroes.size();
    }

    @Mapping(source = "superHeroes", target = "nbHero", qualifiedByName = "getSize")
    public abstract PowerDTO map(Power power);

    abstract List<PowerDTO> map(List<Power> powers);
}
