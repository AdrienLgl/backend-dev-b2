package com.campusacademy.b2dev.backenddevelopmentsupport.api;

import com.campusacademy.b2dev.backenddevelopmentsupport.api.model.PowerDTO;
import com.campusacademy.b2dev.backenddevelopmentsupport.api.model.PowerMinimalDTO;
import com.campusacademy.b2dev.backenddevelopmentsupport.api.model.SuperHeroDTO;
import com.campusacademy.b2dev.backenddevelopmentsupport.api.model.SuperHeroMinimalDTO;
import com.campusacademy.b2dev.backenddevelopmentsupport.model.Power;
import com.campusacademy.b2dev.backenddevelopmentsupport.model.SuperHero;
import com.campusacademy.b2dev.backenddevelopmentsupport.repositories.SuperHeroRepository;
import com.campusacademy.b2dev.backenddevelopmentsupport.repositories.VilainRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequiredArgsConstructor
@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "superHeroes")
public class SuperHeroesController {

    private final SuperHeroRepository superHeroRepository;
    private final VilainRepository vilainRepository;

    @GetMapping
    public ResponseEntity<List<SuperHeroDTO>> getAll() {
        return ResponseEntity.ok(this.superHeroRepository.findAll()
                .stream()
                .map(this::map)
                .collect(Collectors.toList())
        );
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<SuperHeroDTO> getById(@PathVariable Long id) {
        return this.superHeroRepository.findById(id)
                .map(superHero -> ResponseEntity.ok(map(superHero)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<SuperHeroDTO> create(@RequestBody SuperHeroDTO superHeroDTO) {
        SuperHero superHero = map(superHeroDTO);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(map(this.superHeroRepository.save(superHero)));
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<SuperHeroDTO> update(@PathVariable Long id, @RequestBody SuperHeroDTO superHeroDTO) {
        Optional<SuperHero> superHeroOptional = this.superHeroRepository.findById(id);
        if (superHeroOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        SuperHero superHeroToUpdate = map(superHeroDTO);
        return ResponseEntity.ok(map(this.superHeroRepository.save(superHeroToUpdate)));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        this.superHeroRepository.deleteById(id);
    }

    private SuperHeroDTO map(@NonNull SuperHero superHero) {
        List<SuperHeroMinimalDTO> sidekicks = new ArrayList<>();
        if (superHero.getSidekicks() != null) {
            sidekicks = superHero.getSidekicks()
                    .stream()
                    .map(this::mapMinimal)
                    .collect(Collectors.toList());
        }

        List<PowerMinimalDTO> powers = new ArrayList<>();
        if (superHero.getPowers() != null) {
            powers = superHero.getPowers()
                    .stream()
                    .map(this::mapMinimal)
                    .collect(Collectors.toList());
        }

        return new SuperHeroDTO(
                superHero.getId(),
                superHero.getSuperHeroName(),
                superHero.getSecretIdentity(),
                superHero.getNemesis(),
                superHero.getMentor() != null ? mapMinimal(superHero.getMentor()) : null,
                sidekicks,
                powers
        );
    }

    private SuperHeroMinimalDTO mapMinimal(@NonNull SuperHero superHero) {
        return new SuperHeroMinimalDTO(
                superHero.getId(),
                superHero.getSuperHeroName()
        );
    }

    private SuperHero map(@NonNull SuperHeroDTO superHeroDTO) {
        SuperHero superHero = new SuperHero();
        superHero.setSuperHeroName(superHeroDTO.getSuperHeroName());
        superHero.setSecretIdentity(superHeroDTO.getSecretIdentity());
        if (superHeroDTO.getNemesisId() != null) {
            superHero.setNemesis(vilainRepository.findById(superHeroDTO.getNemesisId())
                    .orElse(null));
        }
        return superHero;
    }

    private PowerMinimalDTO mapMinimal(@NonNull Power power) {
        return new PowerMinimalDTO(
                power.getId(),
                power.getName()
        );
    }
}
