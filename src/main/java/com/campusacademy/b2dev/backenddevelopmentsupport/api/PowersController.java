package com.campusacademy.b2dev.backenddevelopmentsupport.api;

import com.campusacademy.b2dev.backenddevelopmentsupport.api.mapper.PowerMapper;
import com.campusacademy.b2dev.backenddevelopmentsupport.api.model.PowerDTO;
import com.campusacademy.b2dev.backenddevelopmentsupport.model.Power;
import com.campusacademy.b2dev.backenddevelopmentsupport.repositories.PowerRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "powers")
// /powers
public class PowersController {

    private final PowerRepository powerRepository;
    private final PowerMapper mapper;

    @GetMapping
    // /powers
    public ResponseEntity<List<PowerDTO>> getAll() {
        // List<PowerDTO> powerDTOs = new ArrayList<>();
        // List<Power> powers = this.powerRepository.findAll();
        // powers.forEach(power -> powerDTOs.add(new PowerDTO(1L, "power1")));
        // return ResponseEntity.ok(powerDTOs);

        return ResponseEntity.ok(this.powerRepository.findAll()
                .stream()
                // .map(p -> this.mapper.map(p))
                .map(mapper::map)
                .collect(Collectors.toList())
        ); // ResponseEntity<List<PowerDTO>>
    }

    @GetMapping(path = "{id}")
    // /powers/{id}
    public ResponseEntity<PowerDTO> getById(@PathVariable Long id) {
        // Code non robuste :
        // Power power = this.powerRepository.getOne(id);
        // return ResponseEntity.ok(new PowerDTO(power.getId(), power.getName()));

        // Code robuste mais verbeux:
        // Optional<Power> powerOptional = this.powerRepository.findById(id);
        // if (powerOptional.isPresent()) {
        //     Power power = powerOptional.get();
        //     return ResponseEntity.ok(new PowerDTO(power.getId(), power.getName()));
        // } else {
        //     return ResponseEntity.notFound().build();
        // }

        // Code robuste et non verbeux:
        return this.powerRepository.findById(id)
                .map(power -> ResponseEntity.ok(map(power)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    // /powers
    public ResponseEntity<PowerDTO> create(@RequestBody PowerDTO powerDTO) {
        Power power = new Power();
        power.setName(powerDTO.getName());
        power.setDescription(powerDTO.getDescription());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(map(this.powerRepository.save(power)));
    }

    @PutMapping(path = "{id}")
    // /powers/{id}
    public ResponseEntity<PowerDTO> update(@PathVariable Long id, @RequestBody PowerDTO powerDTO) {
        Optional<Power> powerOptional = this.powerRepository.findById(id);
        if (powerOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Power powerToUpdate = powerOptional.get();
        powerToUpdate.setName(powerDTO.getName());
        powerToUpdate.setDescription(powerDTO.getDescription());

        return ResponseEntity.ok(map(this.powerRepository.save(powerToUpdate)));
    }

    @DeleteMapping(path = "{id}")
    // /powers/{id}
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        this.powerRepository.deleteById(id);
    }

    private PowerDTO map(@NonNull Power power) {
        int size = 0;

        if (power.getSuperHeroes() != null) {
            size = power.getSuperHeroes().size();
        }

        return new PowerDTO(
                power.getId(),
                power.getName(),
                power.getDescription(),
                size
        );
    }

    // Next Step
    // 1- POST => OK
    // 2- PUT => OK
    // 3- DELETE => OK
    // 4- Ajout filtre GetAll
}
