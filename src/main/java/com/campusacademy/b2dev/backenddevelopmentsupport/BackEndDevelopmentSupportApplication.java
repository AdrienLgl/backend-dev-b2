package com.campusacademy.b2dev.backenddevelopmentsupport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackEndDevelopmentSupportApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackEndDevelopmentSupportApplication.class, args);
	}

	public BackEndDevelopmentSupportApplication() {
	}
}
